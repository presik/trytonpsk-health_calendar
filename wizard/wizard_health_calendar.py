from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class CreateAppointmentStart(ModelView):
    'Create Appointments Start'
    __name__ = 'health.calendar.create.appointment.start'

    healthprof = fields.Many2One('health.professional', 'Health Prof',
        required=True)
    specialty = fields.Many2One('health.specialty', 'Specialty',
        required=True)
    institution = fields.Many2One('health.institution', 'Institution',
        required=True)
    date_start = fields.Date('Start Date', required=True)
    date_end = fields.Date('End Date', required=True)
    time_start = fields.Time('Start Time', required=True, format='%H:%M')
    time_end = fields.Time('End Time', required=True, format='%H:%M')
    appointment_minutes = fields.Integer('Appointment Minutes', required=True)
    monday = fields.Boolean('Monday')
    tuesday = fields.Boolean('Tuesday')
    wednesday = fields.Boolean('Wednesday')
    thursday = fields.Boolean('Thursday')
    friday = fields.Boolean('Friday')
    saturday = fields.Boolean('Saturday')
    sunday = fields.Boolean('Sunday')

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    @fields.depends('healthprof')
    def on_change_with_specialty(self):
        # Return the Current / Main speciality of the Health Professional
        # if this speciality has been specified in the HP record.
        if (self.healthprof and self.healthprof.main_specialty):
            specialty = self.healthprof.main_specialty.specialty.id
            return specialty


class CreateAppointment(Wizard):
    'Create Appointment'
    __name__ = 'health.calendar.create.appointment'

    start = StateView('health.calendar.create.appointment.start',
        'health_calendar.create_appointment_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_', 'tryton-ok', default=True),
        ])
    create_ = StateTransition()
    open_ = StateAction('health.action_health_appointment_view')

    @classmethod
    def __setup__(cls):
        super(CreateAppointment, cls).__setup__()

    def transition_create_(self):
        pool = Pool()
        Appointment = pool.get('health.appointment')
        Company = pool.get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)
            else:
                raise UserError(gettext('health_calendar.msg_no_company_timezone'))
        else:
            raise UserError(gettext('health_calendar.msg_no_user_company_related'))

        appointments = []

        # Iterate over days
        day_count = (self.start.date_end - self.start.date_start).days + 1

        # Validate dates
        if (self.start.date_start and self.start.date_end):
            if (self.start.date_end < self.start.date_start):
                raise UserError(gettext('health_calendar.msg_end_before_start'))

            if (day_count > 31):
                raise UserError(gettext('health_calendar.msg_period_too_long'))

        minutes = self.start.appointment_minutes
        for single_date in (self.start.date_start + timedelta(n)
            for n in range(day_count)):
            if ((single_date.weekday() == 0 and self.start.monday)
                or (single_date.weekday() == 1 and self.start.tuesday)
                or (single_date.weekday() == 2 and self.start.wednesday)
                or (single_date.weekday() == 3 and self.start.thursday)
                or (single_date.weekday() == 4 and self.start.friday)
                or (single_date.weekday() == 5 and self.start.saturday)
                or (single_date.weekday() == 6 and self.start.sunday)):
                # Iterate over time
                dt = datetime.combine(
                    single_date, self.start.time_start)
                dt = timezone.localize(dt)
                dt = dt.astimezone(pytz.utc)
                dt_end = datetime.combine(
                    single_date, self.start.time_end)
                dt_end = timezone.localize(dt_end)
                dt_end = dt_end.astimezone(pytz.utc)
                while dt < dt_end:
                    appointment = {
                        'healthprof': self.start.healthprof.id,
                        'speciality': self.start.specialty.id,
                        'institution': self.start.institution.id,
                        'appointment_date': dt,
                        'appointment_date_end': dt + timedelta(minutes=minutes),
                        'state': 'free',
                    }
                    appointments.append(appointment)
                    dt += timedelta(minutes=minutes)
        if appointments:
            Appointment.create(appointments)
        return 'open_'

    def do_open_(self, action):
        action['pyson_domain'] = [
            ('healthprof', '=', self.start.healthprof.id),
            ('appointment_date', '>=',
                datetime.combine(self.start.date_start, time())),
            ('appointment_date', '<=',
                datetime.combine(self.start.date_end, time())),
            ]
        action['pyson_domain'] = PYSONEncoder().encode(action['pyson_domain'])
        action['name'] += ' - %s' % self.start.healthprof.party.name
        return action, {}

    def transition_open_(self):
        return 'end'
