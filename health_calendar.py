
from datetime import timedelta
from trytond.model import fields
from trytond.pool import PoolMeta


class Appointment(metaclass=PoolMeta):
    __name__ = 'health.appointment'
    appointment_date_end = fields.DateTime('End Date and Time', required=True,
        depends=['appointment_date'])

    @fields.depends('appointment_date')
    def on_change_appointment_date(self, name=None):
        if not self.appointment_date:
            return

        self.appointment_date_end = self.appointment_date + timedelta(hours=1)
